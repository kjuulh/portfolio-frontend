#################
#### BUILDER ####
#################

# base image
FROM node:10.15.1 as BUILDER

# install chrome for protractor tests
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
RUN apt-get update && apt-get install -yq google-chrome-stable

# set working directory
WORKDIR /usr/src/app

# add to path
ENV PATH node_modules/.bin:$PATH

# install and cache app dependencies
COPY front/package.json package.json
RUN npm install
RUN npm install -g @angular/cli

# add app
COPY ./front/ .

# run tests
#RUN ng test --watch=false

# generate build
RUN npm run build

####################
#### PRODUCTION ####
####################

# base image
FROM nginx:1.15.8-alpine

RUN rm -rf /etc/nginx/conf.d
COPY conf /etc/nginx

# copy artifact build from the build environment
COPY --from=BUILDER /usr/src/app/dist/front /usr/share/nginx/html

# expose 80
EXPOSE 80

# run nginx
CMD ["nginx", "-g", "daemon off;"]