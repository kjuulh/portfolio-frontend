#!/bin/bash

echo "Preparing to regenerate portfolio frontend"
echo "Building portfolio-front..."
docker build -t portfolio-front:latest -f Dockerfile.prod .
echo "Tagging portfolio-front..."
docker tag portfolio-front kasperhermansen/portfolio-front:latest
echo "Pushing portfolio-front..."
docker push kasperhermansen/portfolio-front:latest
echo "Ending process"